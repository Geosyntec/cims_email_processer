#general libraries
import os, sys, re, shutil
import traceback
import logging


### TO DO:
# Think about what to do when handling files with the same attachment name??

#configuration and processing
from src.config import mailbox_pending, mailbox_processed, target_dir, archive_dir, error_dir, output_dir, dfr_template_path

from src.dfr_parser import process_dfr
from src.response_email_ms_graph import responseEmail

config_dir = r"C:\Users\vmadmin\Desktop\Geosyntec\ms-graph-api-utils"
sys.path.append(config_dir)

import graph_api_py as gapi

#regex code to parse the uid of the email
pattern_uid = re.compile('\d+ \(UID (?P<uid>\d+)\)')

def parse_uid(data):
    match = pattern_uid.match(data)
    return match.group('uid')

def stageEmails(fromFolder, destFolder, target_dir):
    # Process emails
    # Inputs:
    #   fromFolder: string - Name of "Pending" Folder
    #   destFolder: string - Name of "Success" Folder
    #   target_dir: string - Name of the attachments folder

    #save Attachments
    emails = gapi.download_attachments(1000, [], [], True, target_dir, fromFolder)

    email_list = []
    email_dict = {}

    for x in emails:

        gapi.move_email(x['id'],destFolder)

        #Create the dictionary with email information    
        email_dict = {
        'sender' : x['from']['emailAddress']['address'],
        'subject' : x['subject'],
        'attachments' : [y['name'] for y in x['attachment_data']],
        'list_success_files' : [],
        'list_archive_files' : [],
        'list_error_files' : [],
        'list_errors': []}

        #Create list of email dictionaries
        email_list.append(email_dict)

    #if no messages returned - exit
    if len(emails) == 0:
        logging.info(f"No emails to process!")
        return None, [], None
 
    logging.info("Emails processed and {} messages moved".format(len(emails)))
    return "{} messages moved".format(len(emails)), email_list, emails


def processEmails(email_info, target_dir, archive_dir, error_dir):
    # For a given list of emails, process and respond

    # email_dict = {
    # 'sender' : sender,
    # 'subject' : sub,
    # 'attachments' : attach_list,
    # 'list_success_files' : None,
    # 'list_archive_files' : None
    # 'list_error_files' : None,
    # 'list_errors': None}

    #Build list
    #listFiles = []
    #records = 0

    #Loop through emails
    for x in email_info:
        need_to_process = []
        list_success_files = []
        list_archive_files = []
        list_error_files = []
        list_errors = []

        #Grab attachments
        for y in x['attachments']:
            #filename = os.path.join(target_dir, y)
            need_to_process.append(y)


        for attachment in need_to_process:

            try:
                target_file = os.path.join(target_dir,attachment)
                archive_file =os.path.join(archive_dir,attachment)

                #create dfr
                processed_dfr = process_dfr(target_file, dfr_template_path, output_dir)
                shutil.move(target_file, archive_file)

                #Add DFR to list of successfully processed file
                list_success_files.append(processed_dfr)
                #Add Excel file to the archive list:
                list_archive_files.append(attachment)
                logging.info("Successfully processed: {}".format(attachment))
                logging.info("Successfully generated: {}".format(processed_dfr))

            except Exception as e:
                target_file = os.path.join(target_dir,attachment)
                error_file =os.path.join(error_dir,attachment)
                error = 'Error processing file: ' + str(attachment) + '. Error: {}'.format(e)
                list_error_files.append(attachment)
                list_errors.append(error)
                shutil.move(target_file, error_file)
                logging.info(error)

        x['list_success_files'] = list_success_files
        x['list_archive_files'] = list_archive_files
        x['list_error_files'] = list_error_files
        x['list_errors'] = list_errors

    return email_info

if __name__ == "__main__": 
    #log in
    try:
        gapi.refresh_access_token()

        #logging
        filename = 'CIMS_DFR_processing_log.log'
        logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s',filename=filename, level=logging.INFO)

        #Process the Emails
        result,email_info,raw_email_res = stageEmails(mailbox_pending, mailbox_processed, target_dir)

        if len(email_info) != 0:

            #Process the attachments
            email_processed = processEmails(email_info, target_dir, archive_dir, error_dir)

            #Send Upload information emails
            response = responseEmail(email_processed)

    except Exception as e:
        logging.info("PROCESSING FAILED!!! ")
        logging.info("Error: {}".format(e))
        #logging.info("".join(traceback.format_exception(*sys.exc_info())))
        # ... exit or deal with failure...
        
    

