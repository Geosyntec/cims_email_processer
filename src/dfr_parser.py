# DFR Parser
# 
# To do:
# * Confirm with Chris L. about using the revised DFR template
# * Lock spreadsheet
# * Fix hour subtotal 
# * Add daily summary sheed to Excel and Word Template
# * Add Weather scraper
# * Add ability to merge multiple DFR spreadsheets into one (need to think about Asynchronous nature of this)

# Libraries

import sys
import os
import glob
import datetime
import pandas as pd
import numpy as np
import unicodedata
import string
from jinja2 import Environment, FileSystemLoader
#word report templates
#http://docxtpl.readthedocs.io/en/latest/
#https://github.com/elapouya/python-docx-template/blob/master/tests/richtext.py
from docxtpl import DocxTemplate, RichText

import itertools
from collections import defaultdict

#Additional parser config
date_str_format = "%A, %d %B %Y"    #How the date is formatted in the DFR Word document itself
file_str_format = "%Y-%m-%d"        #How the date is formatted in the DFR Word Filename

# Functions

#https://gist.github.com/wassname/1393c4a57cfcbf03641dbc31886123b8
valid_filename_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
char_limit = 255

def clean_filename(filename, whitelist=valid_filename_chars, replace='_'):
    # replace spaces
    for r in replace:
        filename = filename.replace(r,'_')
    
    # keep only valid ascii chars
    cleaned_filename = unicodedata.normalize('NFKD', filename).encode('ASCII', 'ignore').decode()
    
    # keep only whitelisted chars
    cleaned_filename = ''.join(c for c in cleaned_filename if c in whitelist)
    if len(cleaned_filename)>char_limit:
        print("Warning, filename truncated because it was over {}. Filenames may no longer be unique".format(char_limit))
    return cleaned_filename[:char_limit]  

def descrip_builder(start, end, description, lastrow=False):
    descrip = ''
    if start and start != '':
        descrip += "{} - ".format(start.strftime("%H:%M") )
    if end and end != '':
        descrip += "{} - ".format(end.strftime("%H:%M") )
    if description and lastrow == False:
        descrip += "{}<w:br/><w:br/>".format(description)
    if description and lastrow == True:
        descrip += "{}".format(description)
    return descrip

def format_personnel(Personnel, Abbrev, Author="No"):
    #print(Personnel)
    if Author == "Yes":
        personnel = "{} ({} - Author)".format(Personnel, Abbrev)
    else:
        personnel = "{} ({})".format(Personnel, Abbrev)
    return personnel

#if needed - not currently used
def merge(shared_key, *iterables):
    result = defaultdict(dict)
    for dictionary in itertools.chain.from_iterable(iterables):
        result[dictionary[shared_key]].update(dictionary)
    for dictionary in result.values():
        dictionary.pop(shared_key)
    return result


def subhour_builder(stats_list):  
    if stats_list and len(stats_list)>0:
        str_sub_stats = ""
        for stat in stats_list:
            str_sub_stats += "{} - {} hours <w:br/>".format(stat["Company"],stat["Hours"])
        
        
    else:
        str_sub_stats = "No subcontractor hours."
    return str_sub_stats.strip()

# # Code that does stuff

def process_dfr(file, dfr_template_path, output_dir):
    #Processes file
    

    #Project Info
    df = pd.read_excel(file,sheet_name="Project Info",header=0)
    first_cell_contents = list(df.columns[[0]])
    df_proj_info = df.set_index(first_cell_contents).T
    #rename columns
    df_proj_info.columns = df_proj_info.columns.str.replace(' ', '_')
    #convert to dictionary
    dict_proj_info = df_proj_info.to_dict('records')[0]
    #format the date timestamp if it's a date
    if type(dict_proj_info["Date"]) == str:
        dict_proj_info["Date_str"] = dict_proj_info["Date"]
    else:
        dict_proj_info["Date_str"] = dict_proj_info["Date"].strftime(date_str_format)

    
    # Daily Entries
    #daily entries - fill empty entries with ''
    df_entries = pd.read_excel(file,sheet_name="Description",header=1).fillna('')
    #loop through the daily enties
    entries = ""
    if df_entries.shape[0] > 0:
        for i, row in enumerate(df_entries.itertuples(), 1):
            if i != len(df_entries):
                entries += descrip_builder(row.Start, row.End, row.Description, lastrow=False)
            if i == len(df_entries):
                entries += descrip_builder(row.Start, row.End, row.Description, lastrow=True)
    dict_entries = {"entries" : entries.strip()}

    # Daily Summary
    df_summary = pd.read_excel(file,sheet_name="Summary").fillna('')
    df_summary.columns = df_summary.columns.str.replace(' ', '_')
    df_summary.columns = df_summary.columns.str.lower()
    dict_summary = df_summary.to_dict('records')[0]
    #replace new lines with appropriate breaklines
    dict_summary["daily_summary"]= dict_summary["daily_summary"].replace("\n","<w:br/><w:br/>")

    # Companies
    df_companies = pd.read_excel(file,sheet_name="Contractors",header=1)
    #loop through the companies
    companies = ""
    len_df = len(df_companies)
    for i, row in enumerate(df_companies.itertuples(), 1):
        if i != len_df:
            companies += "{} ({}) / ".format(row.Company, row.Abbrev)
        if i == len_df:
            companies+= "{} ({})".format(row.Company, row.Abbrev)
    dict_companies = {"companies" : companies.strip()}

    # Geosyntec Personnel
    df_geo =  pd.read_excel(file,sheet_name="Geosyntec Personnel",header=1)
    df_geo['Author'] = df_geo['Author'].str.title()
    df_geo["Personnel_Formatted"] = df_geo.apply(lambda row:  format_personnel(row.Personnel, row.Abbrev, row.Author), axis=1)
    dict_geo = {"tbl_geosyntec": df_geo.to_dict('records')}

    #author dictionary
    df_author = df_geo.query("Author == 'Yes'")
    if df_author.shape[0] > 0:
        dict_author = df_author[["Personnel","Position","Abbrev"]].to_dict('records')[0]
        dict_author['geo_author'] = dict_author.pop('Personnel')
        dict_author['geo_position'] = dict_author.pop('Position')
        dict_author['geo_abbrev'] = dict_author.pop('Abbrev')
    else:
        dict_author = {"geo_author": "N/A", "geo_position": "N/A", "geo_abbrev": "NA"}

    # Geosyntec Hours
    geo_hours = (df_geo.groupby(["Company"])["Hours"]
                                .max()
                                .reset_index()
                                .to_dict('records')
                           )[0]["Hours"]
    dict_geo_hours = {"geo_hour_stats" :"GEO - {} hours".format(geo_hours)}


    # Contractors
    df_contractors = pd.read_excel(file,sheet_name="Subcontractors",header=1)

    if df_contractors.shape[0] > 0:
        df_contractors["Personnel_Formatted"] = df_contractors.apply(lambda row:  format_personnel(row.Personnel, row.Abbrev), axis=1)
        dict_sub = {"tbl_subs": df_contractors.to_dict('records')}
        #Subcontractor Hours
        list_contractors_stats = (df_contractors.groupby(["Company"])["Hours"]
                                    .max()
                                    .reset_index()
                                    .to_dict('records')
                               )
        dict_sub_hours = {"sub_hour_stats" : subhour_builder(list_contractors_stats)}
        #merge('Company',list_contractors_stats,list_geo_stats)
    else:
        dict_sub = {"tbl_subs": [
            {"Company": "N/A", 
            "Personnel_Formatted": "N/A",
            "Position": "N/A",
            "Hours": "N/A",
            "Week": "N/A"
            }
            ]}
        dict_sub_hours = {"sub_hour_stats": "N/A"}

    # Visitors
    df_visitors = pd.read_excel(file,sheet_name="Visitors",header=1).dropna().fillna('')

    if df_visitors.shape[0] > 0:
        df_visitors["Personnel_Formatted"] = df_visitors.apply(lambda row:  format_personnel(row.Personnel, row.Abbrev), axis=1)
        dict_visitors = {"tbl_visitors": df_visitors.to_dict('records')}
    else:
        dict_visitors = {"tbl_visitors": [{"Company": "N/A", "Personnel_Formatted": "N/A"}]}

    # Equipment
    df_equipment = pd.read_excel(file,sheet_name="Equipment",header=1).fillna('')
    df_equipment.columns = df_equipment.columns.str.replace(' ', '_')
    df_equipment['Usage'] = df_equipment['Usage'].str.title()
    dict_equipment = {"tbl_equipment": df_equipment.query("Usage == 'Used'").to_dict('records')}

    #Build filename
    if isinstance(dict_proj_info["Date"], datetime.date):
        docOutfile = clean_filename("{}-{}.docx".format(dict_proj_info["Date"].strftime(file_str_format),dict_author['geo_abbrev']))
    else:
        docOutfile = clean_filename("{}-{}.docx".format(dict_proj_info["Date_str"],dict_author['geo_abbrev']))
    #docOutfile = "test.docx"
    print("Generating {}".format(docOutfile))

    dict_Outfile = {"filename": docOutfile}

    # Build Template Variables by combining all the dictionaries
    template_vars = {**dict_proj_info,**dict_entries,**dict_equipment,
                    **dict_companies, **dict_geo, ** dict_sub, **dict_visitors, 
                     **dict_geo_hours,**dict_sub_hours, **dict_author, **dict_summary, **dict_Outfile}



    doc = DocxTemplate(dfr_template_path)



    doc.render(template_vars)    
    doc.save(os.path.join(output_dir,docOutfile))
    doc = None

    return docOutfile




if __name__ == "__main__":
    print("Running Test...")
    #test_file = 'DFR_Test.xlsx'
    test_file = r'C:\Users\rsiebenmann\OneDrive - Geosyntec\GitHub\cims_email_processer\attachments\GW6489-2019-02-01-DW.xlsx'

    processed_dfr = process_dfr(test_file)
    print("File Processed... {}".format(processed_dfr))