import os
import email, smtplib, ssl
from email import encoders
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from src.config import toaddresses, subject, projectName, output_dir, error_dir, archive_dir

def responseEmail(email_to_process, imap_server, imap_user, imap_password):

    # This Function will process and send emails.
    # Input is a list of dictionaries with the following format:
    # email_dict = {
    # 'sender' : sender,
    # 'subject' : sub,
    # 'attachments' : attach_list,
    # 'list_success_files' : None,
    # 'list_archive_files' : None
    # 'list_error_files' : None,
    # 'list_errors': None}

    for x in email_to_process:

        print(x)
        try:
            #append original sender email to generic to list
            toaddresses.append(x['sender'])

            #build message
            message = MIMEMultipart("alternative")
            message["Subject"] = subject
            message["From"] = imap_user
            message["To"] = ", ".join(toaddresses)

            # Create the plain-text and HTML version of your message
            text = 'Plant Barry DFR Processing'

            body_text = 'Files Attached: ' + ', '.join(x['attachments'])


            body_html = "<br><span style=\' COLOR: black; FONT-SIZE: 14pt;\'><b>DFRs Processed: </b>" \
                     "<span>Files Attached: " + ', '.join(x['attachments']) + "</span><br><br>" \
                     "<span>Files Successfully Processed: " + ', '.join(x['list_success_files']) + "</span>" 
            html = """\
            <html>
            <body>
            """
            html += body_html


            # #Failure:
            if len(x['list_errors']) > 0:
                fail_html = "<br><br><br><span>Files With Errors: " + ', '.join(x['list_errors']) + "</span>"
                html += fail_html

            close_html = """\
            </body>
            </html>
            """
            html += close_html

            # Turn these into plain/html MIMEText objects
            part1 = MIMEText(text, "plain")
            part2 = MIMEText(html, "html")

            # Add HTML/plain-text parts to MIMEMultipart message
            # The email client will try to render the last part first
            message.attach(part1)
            message.attach(part2)

            #add successful attachments
            for success_file in x['list_success_files']:

                # Open attachment file in binary mode
                filename = os.path.join(output_dir,success_file)

                with open(filename, "rb") as attachment:
                    # Add file as application/octet-stream
                    # Email client can usually download this automatically as attachment
                    part = MIMEBase("application", "octet-stream")
                    part.set_payload(attachment.read())

                # Encode file in ASCII characters to send by email    
                encoders.encode_base64(part)

                # Add header as key/value pair to attachment part
                part.add_header(
                    "Content-Disposition",
                    f"attachment; filename= {success_file}",
                )

                # Add attachment to message 
                message.attach(part)

            #add error attachments
            for error_file in x['list_error_files']:

                # Open attachment file in binary mode
                filename = os.path.join(error_dir,error_file)

                with open(filename, "rb") as attachment:
                    # Add file as application/octet-stream
                    # Email client can usually download this automatically as attachment
                    part = MIMEBase("application", "octet-stream")
                    part.set_payload(attachment.read())

                # Encode file in ASCII characters to send by email    
                encoders.encode_base64(part)

                # Add header as key/value pair to attachment part
                part.add_header(
                    "Content-Disposition",
                    f"attachment; filename= {error_file}",
                )

                # Add attachment to message 
                message.attach(part)

            #add the archive attachments
            for archive_file in x['list_archive_files']:

                # Open attachment file in binary mode
                filename = os.path.join(archive_dir,archive_file)

                with open(filename, "rb") as attachment:
                    # Add file as application/octet-stream
                    # Email client can usually download this automatically as attachment
                    part = MIMEBase("application", "octet-stream")
                    part.set_payload(attachment.read())

                # Encode file in ASCII characters to send by email    
                encoders.encode_base64(part)

                # Add header as key/value pair to attachment part
                part.add_header(
                    "Content-Disposition",
                    f"attachment; filename= {archive_file}",
                )

                # Add attachment to message 
                message.attach(part)
            # convert message to string
            text = message.as_string()

            print(toaddresses)
            # The actual mail send
            with smtplib.SMTP(imap_server) as server:
                server.starttls()
                server.login(imap_user,imap_password)
                server.sendmail(imap_user, toaddresses, text)
                server.quit()



            # #Header    
            # strHeader = "<html><head></style></head><body lang=EN-US link=black vlink=purple><div class=Section1>" \
            #         "</HEAD><DIV class=Section1><P class=section1 style='MARGIN: 0in 0in 0pt; TEXT-ALIGN: left'" \
            #         " align=left><EM><B><I><FONT face=Calibri color=black size=12pt><SPAN" \
            #         " Style = 'FONT-WEIGHT: bold; FONT-SIZE: 24pt; COLOR: black; FONT-FAMILY: Calibri' >" + str(projectName) + \
            #         "</SPAN></FONT></I></B></EM><o:p></o:p></P><P class=section1 style='MARGIN: 0in 0in 0pt; TEXT-ALIGN: left'" \
            #         " align=left><EM><B><I><FONT face=Calibri color=black size=12pt><SPAN Style = 'FONT-WEIGHT: bold; FONT-SIZE: 16pt; " \
            #         "COLOR: black; FONT-FAMILY: Calibri' >DFR Processing</SPAN></FONT></I></B></EM><o:p></o:p></P>" 

					
            # #Email Body        
            # BodyStr = ""

            # BodyStr = "<br><span style=\' COLOR: black; FONT-SIZE: 14pt;\'><b>DFRs Processed: </b>" \
            #         "<span>Files Attached: " + ', '.join(x['attachments']) + "</span><br><br>" \
            #         "<span>Files Successfully Processed: " + ', '.join(x['list_success_files']) + "</span>" 
                    

            # #Failure:
            # failStr = "<span>Files With Errors: " + ', '.join(x['list_errors']) + "</span>"

            # if len(x['list_errors']) == 0:
            #     html = strHeader + BodyStr
            # else:
            #     html = strHeader + BodyStr + failStr

            # #msg.attach(MIMEText(body, 'plain')) - if this is on the html will send in an attachement
            # msg.attach(MIMEText(html, 'html'))

            # #Send Email - This was how the data check was being performed so I stuck with that for now
            # server = smtplib.SMTP(imap_server)
            # server.ehlo()
            # server.starttls()
            # server.ehlo()
            # server.login(imap_user, imap_password)
            # text = msg.as_string()
            # print(toaddresses)
            # server.sendmail(imap_user, toaddresses, text)

        except Exception as e:
            return e


    return 'Success'