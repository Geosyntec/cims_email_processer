import glob
import os
import time
import datetime
import pandas as pd
import shutil

from sql_functions import revupSQLEngine
from sql_functions import clean_df_db_dups
from sqlalchemy import create_engine

import config

# Functions
def merge_cols(a,b):
    return "{}:{}".format(a,b)


def process_file(email_info):
    # Input Parameters
    target_dir = os.getcwd() + config.target_dir
    archive_dir = config.archive_dir
    error_dir = config.error_dir
    mssqlDestinationTable = config.destinationTable
    mySQLsuccess = False

    #need_to_process = glob.glob(os.path.join(target_dir, '*.csv*'))

    sqlEngine = revupSQLEngine()

    #Process files in target folder
    listFiles = []
    records = 0

    for x in email_info:
        
        need_to_process = []
        listFiles = []

        for y in x['attachments']:
            filename = os.path.join(target_dir, y)
            need_to_process.append(filename)

        for files in need_to_process:

            filedir, filename = os.path.split(files)
            filebase, fileext = os.path.splitext(filename)
            filetime = datetime.datetime.now().strftime("_%Y%m%d-%H%M")

            output_file = filebase + filetime + '.csv'
            outFile = os.path.join(target_dir + archive_dir, output_file)
            errFile = os.path.join(target_dir + error_dir, output_file)

            try:
                df_raw = pd.read_csv(files,encoding='ISO-8859-1').dropna(subset=['Date/Time']).set_index(["Device ID", "Date/Time"])

                df_unstack = pd.DataFrame(df_raw.stack(), columns=['results_str']).reset_index()

                df_unstack.columns = ["Device_ID", "Date_Time","parameter","results_str"]

                df_unstack["results_val"] = df_unstack["results_str"].apply(pd.to_numeric, errors='coerce')

                df_unstack['unique_key'] = df_unstack.apply(lambda x: merge_cols(x["Device_ID"],x["parameter"]), axis=1)
                
                df_unstack = df_unstack.drop_duplicates(['Device_ID','Date_Time','parameter'], keep='last')

                df_unstack['Date_Time'] =  pd.to_datetime(df_unstack['Date_Time'])

                df2 = clean_df_db_dups(df_unstack, mssqlDestinationTable, sqlEngine, dup_cols=['Device_ID','Date_Time','parameter'], lower_col='parameter')

                records = records + df2.shape[0]

                try:
                    df2.to_sql(mssqlDestinationTable, sqlEngine, if_exists='append', index=False)
                    shutil.move(files, outFile)
                    mySQLsuccess = True
                    error = None
                    listFiles.append(output_file)
                    
                except Exception, e:
                    error = 'Error uploading ' + str(output_file) + ' to SQL database. Error: {}'.format(e)
                    mySQLsuccess = False
                    shutil.move(files, errFile)

            except Exception, e:
                error = 'Error processing file: ' + str(output_file) + '. Error: {}'.format(e)
                mySQLsuccess = False
                shutil.move(files, errFile)

        x['success'] = mySQLsuccess
        x['records'] = records
        x['error'] = error
        x['list_files'] = listFiles


    return email_info
