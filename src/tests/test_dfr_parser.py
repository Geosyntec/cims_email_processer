#test libraries
from pkg_resources import resource_filename

#configurations
from src.dfr_parser import process_dfr
from src.config import dfr_template_path

def test_process_dfr():
        test_filepath = r'C:\Users\rsiebenmann\OneDrive - Geosyntec\GitHub\cims_email_processer\attachments\GW6489-2019-02-01-DW.xlsx'
        test_filename = "GW6489-2019-02-01-DW.xlsx"
        test_file = resource_filename("src.tests.baseline_files", test_filename)
        output_dir = resource_filename("src.tests", "results_files")
        processed_dfr = process_dfr(test_file, dfr_template_path, output_dir)
        print("File Processed... {}".format(processed_dfr))
