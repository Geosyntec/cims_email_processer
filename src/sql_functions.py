import pandas
from sqlalchemy import create_engine
import config

def revupSQLEngine():

    # PMH: putting an f before the string makes it magic
    #      (only for pythons >= 3.6)
    engine_string = ('mssql+pyodbc://' + config.UserName + ':' 
        + config.Password + '@' + config.ServerName + '/' 
        + config.Database + '?driver=SQL+Server+Native+Client+11.0')

    engine = create_engine(engine_string)
    return engine


def clean_df_db_dups(df, tablename, engine, dup_cols=[], lower_col=None,
                         filter_continuous_col=None, filter_categorical_col=None):
    """
    Remove rows from a dataframe that already exist in a database

    Required:
        df : dataframe to remove duplicate rows from
        engine: SQLAlchemy engine object
        tablename: tablename to check duplicates in
        dup_cols: list or tuple of column names to check for duplicate row values

    Returns
        Unique list of values from dataframe compared to database table
    """
    args = 'SELECT %s FROM [%s]' %(', '.join(['{0}'.format(col) for col in dup_cols]), tablename)

    df_dups = pandas.read_sql(args, engine)


    if lower_col is not None:
        merge_cols = [x if x != lower_col else x + '_lower' for x in dup_cols]
        df_dups[lower_col + '_lower'] = df_dups[lower_col].str.lower()
        df[lower_col + '_lower'] = df[lower_col].str.lower()
        df_dups.rename(columns={lower_col:lower_col + '_dfdup'}, inplace=True)

        drop_cols = ['_merge', lower_col + '_dfdup', lower_col + '_lower']

    else:
        merge_cols = dup_cols

        drop_cols = ['_merge']


    #df.drop_duplicates(dup_cols, keep='last', inplace=True)
    df = pandas.merge(df,df_dups, how='left', on=merge_cols, indicator=True)  
    df = df[df._merge == 'left_only'].reset_index(drop=True)
    df.drop(drop_cols, axis=1, inplace=True)
    
    return df