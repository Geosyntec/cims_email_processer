import email, smtplib, ssl

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from config import imap_server, imap_user, imap_password, toaddresses, subject, projectName

message = MIMEMultipart("alternative")
message["Subject"] = "multipart test"
message["From"] = imap_user
message["To"] = ", ".join(toaddresses)

# Create the plain-text and HTML version of your message
text = """\
Hi,
How are you?
Real Python has many great tutorials:
www.realpython.com"""
html = """\
<html>
  <body>
    <p>Hi,<br>
       How are you?<br>
       <a href="http://www.realpython.com">Real Python</a> 
       has many great tutorials.
    </p>
  </body>
</html>
"""

# Turn these into plain/html MIMEText objects
part1 = MIMEText(text, "plain")
part2 = MIMEText(html, "html")

# Add HTML/plain-text parts to MIMEMultipart message
# The email client will try to render the last part first
message.attach(part1)
message.attach(part2)

#attachment
# Open PDF file in binary mode

filename = r"C:\Users\rsiebenmann\OneDrive - Geosyntec\GitHub\cims_email_processer\attachments\output_dir\2019-02-01-DW.docx"
fileout = "2019-02-01-DW.docx"
with open(filename, "rb") as attachment:
    # Add file as application/octet-stream
    # Email client can usually download this automatically as attachment
    part = MIMEBase("application", "octet-stream")
    part.set_payload(attachment.read())
# Encode file in ASCII characters to send by email    
encoders.encode_base64(part)

# Add header as key/value pair to attachment part
part.add_header(
    "Content-Disposition",
    f"attachment; filename= {fileout}",
)

# Add attachment to message and convert message to string
message.attach(part)
text = message.as_string()

print(toaddresses)
# The actual mail send
with smtplib.SMTP(imap_server) as server:
    server.starttls()
    server.login(imap_user,imap_password)
    server.sendmail(imap_user, ", ".join(toaddresses), text)
    server.quit()
