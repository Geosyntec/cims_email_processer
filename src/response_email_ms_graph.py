import os
import sys
import traceback

from src.config import toaddresses, subject , projectName, output_dir, error_dir, archive_dir

config_dir = r"C:\Users\vmadmin\Desktop\Geosyntec\ms-graph-api-utils"
sys.path.append(config_dir)

import graph_api_py as gapi

def responseEmail(email_to_process):#, imap_server, imap_user, imap_password):

    # This Function will process and send emails.
    # Input is a list of dictionaries with the following format:
    # email_dict = {
    # 'sender' : sender,
    # 'subject' : sub,
    # 'attachments' : attach_list,
    # 'list_success_files' : None,
    # 'list_archive_files' : None
    # 'list_error_files' : None,
    # 'list_errors': None}

    for x in email_to_process:

        try:
            #append original sender email to generic to list
            toaddresses.append(x['sender'])

            # Create the plain-text and HTML version of your message
            text = 'Plant Barry DFR Processing'

            body_text = 'Files Attached: ' + ', '.join(x['attachments'])
            
            body_html = "<br><span style=\' COLOR: black; FONT-SIZE: 14pt;\'><b>Files Processed: </b>" \
                     + ', '.join(x['attachments']) + "</span><br><br>" \
                     "<span><b>DFRs Successfully Created:</b> " + ', '.join(x['list_success_files']) + "</span>" 
            html = """\
            <html>
            <body>
            """
            html += body_html

            # #Failure:
            if len(x['list_errors']) > 0:
                fail_html = "<br><br><br><span><b>Files With Errors:</b> " + ', '.join(x['list_errors']) + "</span>"
                html += fail_html

            close_html = """\
            </body>
            </html>
            """
            html += close_html

            attachments = x['list_success_files']

            attachments[:] = [os.path.join(output_dir,y) for y in attachments]


############### SEND EMAIL WITH MS GRAPH
            
            email_parameters = {
                'subject': subject,
                'body': html,
                'recipients': toaddresses,
                'attachments': attachments
            }

            print('Sending email')
            email_response = gapi.send_email(**email_parameters)

            #logging.info("Email sent successfully")

        except Exception as error:
            print('Fail')
            traceback.print_exception(*sys.exc_info())
            return error


    return email_response


#this is for testing

if __name__ == "__main__": 
    #log in
    #This is for testing purposes
    try:

        email_processed = [{
        'sender' : 'bharris@geosyntec.com',
        'subject' : 'Testing New Email Functionality',
        'attachments' : ["Plant Barry DFR 9-26-2022.xlsx","Plant Barry DFR 9-27-2022.xlsx"],
        'list_success_files' : [r"C:\Users\vmadmin\Desktop\Geosyntec\cims_email_processer\attachments\archive_dir\Plant Barry DFR 9-26-2022.xlsx",r"C:\Users\vmadmin\Desktop\Geosyntec\cims_email_processer\attachments\archive_dir\Plant Barry DFR 9-27-2022.xlsx"],
        'list_archive_files' : [],
        'list_error_files' : [],
        'list_errors': []}]

        #print(email_processed)
        #Send Upload information emails
        response = responseEmail(email_processed)#, imap_server, imap_user, imap_password)
        
        print(response)



    except Exception as e:
        print(e)
        # ... exit or deal with failure...