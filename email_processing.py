#general libraries
import os, sys, re, shutil
import imaplib, email, smtplib
import datetime

### TO DO:
# Think about what to do when handling files with the same attachment name??

#configuration and processing
from src.config import imap_server, imap_user, imap_password, mailbox_pending, mailbox_processed, \
                        target_dir, archive_dir, error_dir, output_dir, dfr_template_path

from src.dfr_parser import process_dfr
from src.response_email import responseEmail

#from Unstack import process_file
#from response_email import responseEmail

#regex code to parse the uid of the email
pattern_uid = re.compile('\d+ \(UID (?P<uid>\d+)\)')

def parse_uid(data):
    match = pattern_uid.match(data)
    return match.group('uid')

def stageEmails(imap_server, imap_user, imap_password, fromFolder, destFolder, target_dir):
    # Process emails
    # Inputs:
    #   imap_server: string - server address
    #   imap_user: string - user name
    #   imap_password:  string - password
    #   fromFolder: string - Name of "Pending" Folder
    #   destFolder: string - Name of "Success" Folder
    #   target_dir: string - Name of the attachments folder

    # Make connection to mailbox, con
    con = imaplib.IMAP4_SSL(imap_server)
    con.login(imap_user, imap_password)

    #select emails from a specific folder
    rv, data = con.select(fromFolder)
 
    #if no messages returned - exit
    if rv != 'OK':
        print("No messages found!")
        return "No messages found"
   
    #return the actual emails using a search
    rv, data = con.search(None, "ALL")
  
    #save Attachments
    email_list = save_attachment(con, data, target_dir)
       
    #counter
    iMoved = 0
  
    #loop through the messages and move them
    for num in data[0].split():
        #grab the unique identifier
        rv, data = con.fetch(num, '(UID)')
        msg_uid = parse_uid(data[0].decode('utf-8'))

        #copy to the new folder
        result = con.uid('COPY', msg_uid, destFolder)

        #if move was successful - delete the original email
        if result[0] == 'OK':
            iMoved += 1
            mov, data = con.uid('STORE', msg_uid , '+FLAGS', '(\Deleted)')
            #con.expunge()

    #disconnect
    con.expunge()
    con.logout()
    print("Emails processed and {} messages moved".format(iMoved))        
    return "{} messages moved".format(iMoved), email_list

def save_attachment(con,data, target_dir):
    # for a connection and data package, save attachments
    # generates a dictionary of emails with a list of attachments

    email_list = []
    email_dict = {}

    #Where the attachments will be saved
    save_dir = os.getcwd() + target_dir
    c = 0

    #Process each email in the pending folder and place attachments in the save_dir
    for num in data[0].split():
        typ, data = con.fetch(num, '(RFC822)')
        c +=1
        text = data[0][1]
        msg = email.message_from_string(text.decode('utf-8'))
        sender = msg['From']
        sender = sender[sender.find("<")+1:sender.find(">")]
        sub = msg['Subject']
        attach_list = []

        for part in msg.walk():
            if part.get_content_maintype() == 'multipart':
                continue
            if part.get('Content-Disposition') is None:
                continue
            filename = part.get_filename()
            data = part.get_payload(decode=True)
            if not data:
                continue
            attach_list.append(filename)
            f  = open(os.path.join(save_dir,filename), 'wb')
            f.write(data)
            f.close()
        
        #Create the dictionary with email information    
        email_dict = {
        'sender' : sender,
        'subject' : sub,
        'attachments' : attach_list,
        'list_success_files' : [],
        'list_archive_files' : [],
        'list_error_files' : [],
        'list_errors': []}
        
        #Create list of email dictionaries
        email_list.append(email_dict)
            
    return email_list

def processEmails(email_info, target_dir, archive_dir, error_dir):
    # For a given list of emails, process and respond

    # email_dict = {
    # 'sender' : sender,
    # 'subject' : sub,
    # 'attachments' : attach_list,
    # 'list_success_files' : None,
    # 'list_archive_files' : None
    # 'list_error_files' : None,
    # 'list_errors': None}

    #Build list
    #listFiles = []
    #records = 0

    #Loop through emails
    for x in email_info:
        need_to_process = []
        list_success_files = []
        list_archive_files = []
        list_error_files = []
        list_errors = []

        #Grab attachments
        for y in x['attachments']:
            #filename = os.path.join(target_dir, y)
            need_to_process.append(y)


        for attachment in need_to_process:

            try:
                target_file = os.path.join(target_dir,attachment)
                archive_file =os.path.join(archive_dir,attachment)

                #create dfr
                processed_dfr = process_dfr(target_file, dfr_template_path, output_dir)
                shutil.move(target_file, archive_file)

                #Add DFR to list of successfully processed file
                list_success_files.append(processed_dfr)
                #Add Excel file to the archive list:
                list_archive_files.append(attachment)

            except Exception as e:
                target_file = os.path.join(target_dir,attachment)
                error_file =os.path.join(error_dir,attachment)
                error = 'Error processing file: ' + str(attachment) + '. Error: {}'.format(e)
                list_error_files.append(attachment)
                list_errors.append(error)
                shutil.move(target_file, error_file)

        x['list_success_files'] = list_success_files
        x['list_archive_files'] = list_archive_files
        x['list_error_files'] = list_error_files
        x['list_errors'] = list_errors

    return email_info

if __name__ == "__main__": 
    #log in
    try:
        #Process the Emails
        result,email_info = stageEmails(imap_server, imap_user, imap_password, mailbox_pending, mailbox_processed, target_dir)

        #Process the attachments
        email_processed = processEmails(email_info, target_dir, archive_dir, error_dir)

        print(email_processed)
        #Send Upload information emails
        response = responseEmail(email_processed, imap_server, imap_user, imap_password)
        print(response)



    except imaplib.IMAP4.error:
        print("LOGIN FAILED!!! ")
        # ... exit or deal with failure...
        
    

